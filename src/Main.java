import builder.*;
import builder.modelbuilders.CX5Builder;
import builder.modelbuilders.MX5Builder;
import builder.modelbuilders.OptimaBuilder;
import builder.modelbuilders.RioBuilder;
import builder.origin.Builder;
import factory.AbstractFactory;
import factory.FactoryProducer;
import product.mark.origin.Car;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        while (true) {
            Scanner sc = new Scanner(System.in);
            System.out.println("Choose which car do you want:\n" +
                    "1. Kia\n" +
                    "2. Mazda\n" +
                    "9. Exit\n");
            String carType = "", carModel = "";
            if (sc.hasNextInt()) {
                int number = sc.nextInt();
                if(number == 1) {
                    carType = "Kia";
                } else if(number == 2) {
                    carType = "Mazda";
                } else if(number == 9) {
                    break;
                }
            } else {
                System.out.println("That is not a option! Try again! Tip: You should use numbers from 1-9");
                continue;
            }

            AbstractFactory carFactory = FactoryProducer.getFactory(carType);
            Car car1 = null;
            String s0 = "Which model do you want:\n", s1 = "1. Optima\n2. Rio\n",s2 = "1. CX-5\n2. MX-5\n";

            System.out.println(s0 + ((carType.equals("Kia")) ? s1 : s2) );

            if(carType.equals("Kia")) {

                if (sc.hasNextInt()) {
                    int number = sc.nextInt();
                    if(number == 1) {
                        carModel = "Optima";
                        car1 = carFactory.getCar(carModel);
                    } else if(number == 2) {
                        carModel = "Rio";
                        car1 = carFactory.getCar(carModel);
                    } else if(number == 9) {
                        break;
                    }
                } else {
                    System.out.println("That is not a option! Try again! Tip: You should use numbers from 1-9");
                    continue;
                }

            } else if(carType.equals("Mazda")) {

                if (sc.hasNextInt()) {
                    int number = sc.nextInt();
                    if(number == 1) {
                        carModel = "CX5";
                        car1 = carFactory.getCar(carModel);
                    } else if(number == 2) {
                        carModel = "MX5";
                        car1 = carFactory.getCar(carModel);
                    } else if(number == 9) {
                        break;
                    }
                } else {
                    System.out.println("That is not a option! Try again! Tip: You should use numbers from 1-9");
                    continue;
                }
            }
            Director director = new Director();
            Builder builder = null;
            if(carType.equals("Kia")) {
                if(carModel.equals("Rio")) {
                    builder = new RioBuilder();
                } else if(carModel.equals("Optima")) {
                    builder = new OptimaBuilder();
                }
            } else if(carType.equals("Mazda")) {
                if(carModel.equals("CX5")) {
                    builder = new CX5Builder();
                } else if(carModel.equals("MX5")) {
                    builder = new MX5Builder();
                }
            }

            director.construct(builder);

            car1 = builder.getResult();

            car1.showResult();


        }
    }
}
