package product.cars.mazda;

import product.mark.Mazda;

public class CX5 implements Mazda {
    private String color;
    private boolean transmission, tripComputer;
    private double engine;

    public CX5(String color, boolean transmission, double engine, boolean tripComputer) {
        this.transmission = transmission;
        this.color = color;
        this.tripComputer = tripComputer;
        this.engine = engine;
    }

    @Override
    public void drive() {
        System.out.println("ZHZHZHZHZH\n");
    }

    @Override
    public void showResult() {
        System.out.println(color + " Mazda CX-5 on " + (transmission?"automated":"mechanical") + " transmission has " + engine + " engine and " + (tripComputer?"has":"doesn't have") + " Automated Trip Helper!");
        System.out.println("\n");
    }
}
