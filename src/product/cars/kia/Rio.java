package product.cars.kia;

import product.mark.Kia;

public class Rio implements Kia {
    private String color;
    private boolean transmission, tripComputer;
    private double engine;

    public Rio(String color, boolean transmission, double engine, boolean tripComputer) {
        this.transmission = transmission;
        this.color = color;
        this.tripComputer = tripComputer;
        this.engine = engine;
    }

    @Override
    public void drive() {
        System.out.println("VVVVVVVV\n");
    }

    @Override
    public void showResult() {
        System.out.println(color + " Kia Rio on " + (transmission?"automated":"mechanical") + " transmission has " + engine + " engine and " + (tripComputer?"has":"doesn't have") + " Automated Trip Helper!");
        System.out.println("\n");
    }
}
