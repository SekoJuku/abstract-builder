package product.mark.origin;

public interface Car {
    void drive() ;

    void showResult();
}
