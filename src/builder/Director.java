package builder;

import builder.origin.Builder;

import java.util.Scanner;

public class Director {

    public void construct(Builder builder) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Lets customize it!" +
                "Color :\n" +
                "1. White\n" +
                "2. Gold\n" +
                "3. Black\n" +
                "4. Red\n");
        while (true) {
            if (sc.hasNextInt()) {
                int number = sc.nextInt();
                if (number == 1) {
                    builder.setColor("White");
                    break;
                } else if (number == 2) {
                    builder.setColor("Gold");
                    break;
                } else if (number == 3) {
                    builder.setColor("Black");
                    break;
                } else if (number == 4) {
                    builder.setColor("Red");
                    break;
                } else {
                    System.out.println("Try again!\n");
                }
            }
        }

        System.out.println("Which type of transmission you want:\n" +
                "1. MT\n" +
                "2. AT\n");
        while (true) {
            if(sc.hasNextInt()) {
                int number = sc.nextInt();
                if(number == 1) {
                    builder.setTransmission(false);
                    break;
                } else if(number == 2) {
                    builder.setTransmission(true);
                    break;
                } else {
                    System.out.println("Try again!\n");
                }
            }
        }

        System.out.println("Which volume of engine you want:\n" +
                "1. 2.1\n" +
                "2. 2.5\n");
        while (true) {
            if(sc.hasNextInt()) {
                int number = sc.nextInt();
                if(number == 1) {
                    builder.setEngine(2.1);
                    break;
                } else if(number == 2) {
                    builder.setEngine(2.6);
                    break;
                } else {
                    System.out.println("Try again!\n");
                }
            }
        }

        System.out.println("You want Automated Trip Helper:\n" +
                "1. Yes\n" +
                "2. No\n");
        while (true) {
            if(sc.hasNextInt()) {
                int number = sc.nextInt();
                if(number == 1) {
                    builder.setTripComputer(true);
                    break;
                } else if(number == 2) {
                    builder.setTripComputer(false);
                    break;
                } else {
                    System.out.println("Try again!\n");
                }
            }
        }
    }
}
