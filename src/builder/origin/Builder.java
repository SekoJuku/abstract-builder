package builder.origin;

import product.mark.origin.Car;

public interface Builder {
    void setTransmission(boolean transmission);
    void setColor(String color);
    void setTripComputer(boolean tripComputer);
    void setEngine(double engine);

    Car getResult();
}
