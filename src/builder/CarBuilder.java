package builder;

import builder.origin.Builder;
import product.mark.origin.Car;

public class CarBuilder implements Builder {

    public String color;
    public boolean transmission;
    public boolean tripComputer;
    public double engine;

    @Override
    public void setTransmission(boolean transmission) {
        this.transmission = transmission;
    }

    @Override
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public void setTripComputer(boolean tripComputer) {
        this.tripComputer = tripComputer;
    }

    @Override
    public void setEngine(double engine) { this.engine = engine; }

    @Override
    public Car getResult() {
        return null;
    }

}
