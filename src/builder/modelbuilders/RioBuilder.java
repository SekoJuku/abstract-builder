package builder.modelbuilders;

import builder.CarBuilder;
import product.cars.kia.Rio;
import product.mark.origin.Car;

public class RioBuilder extends CarBuilder {
    public Rio getResult() {
        return new Rio(color,transmission,engine,tripComputer);
    }
}
