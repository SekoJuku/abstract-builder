package builder.modelbuilders;

import builder.CarBuilder;
import product.cars.kia.Optima;
import product.mark.origin.Car;

public class OptimaBuilder extends CarBuilder {
    public Optima getResult() {
        return new Optima(color,transmission,engine,tripComputer);
    }
}
