package builder.modelbuilders;

import builder.CarBuilder;
import product.cars.mazda.MX5;
import product.mark.origin.Car;

public class MX5Builder extends CarBuilder {
    public MX5 getResult() {
        return new MX5(color,transmission,engine,tripComputer);
    }
}
