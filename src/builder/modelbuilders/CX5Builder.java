package builder.modelbuilders;

import builder.CarBuilder;
import product.cars.mazda.CX5;
import product.mark.origin.Car;

public class CX5Builder extends CarBuilder {
    public CX5 getResult() {
        return new CX5(color,transmission,engine,tripComputer);
    }
}
