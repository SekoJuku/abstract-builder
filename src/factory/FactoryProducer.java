package factory;

public class FactoryProducer {

    public static AbstractFactory getFactory(String carMark) {
        if(carMark.equalsIgnoreCase("Kia")) {
            return new KiaFactory();
        } else if(carMark.equalsIgnoreCase("Mazda")) {
            return new MazdaFactory();
        }
        return null;
    }
}
