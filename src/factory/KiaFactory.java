package factory;

import product.cars.kia.Optima;
import product.cars.kia.Rio;
import product.mark.origin.Car;

public class KiaFactory implements AbstractFactory {

    @Override
    public Car getCar(String carType) {
        if(carType.equalsIgnoreCase("Rio")) {
            return new Rio(null,false,0,false);
        } else if(carType.equalsIgnoreCase("Optima")) {
            return new Optima(null,false,0,false);
        }
        return null;
    }
}
