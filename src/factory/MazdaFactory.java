package factory;

import product.cars.mazda.CX5;
import product.cars.mazda.MX5;
import product.mark.origin.Car;

public class MazdaFactory implements AbstractFactory{

    @Override
    public Car getCar(String carType) {
        if(carType.equalsIgnoreCase("MX5")) {
            return new MX5(null,false,0,false);
        } else if(carType.equalsIgnoreCase("CX5")) {
            return new CX5(null,false,0,false);
        }
        return null;
    }
}
