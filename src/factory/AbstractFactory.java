package factory;

import product.mark.origin.Car;

public interface AbstractFactory {
    Car getCar(String carType) ;
}
